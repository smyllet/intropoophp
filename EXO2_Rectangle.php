<?php
    class Rectangle
    {
        // Declaration des attributs:
        public $longueur = 0;
        public $largeur = 0;

        // Définition de la longueur et largeur du rectangle:
        function definitTaille($lg = 0, $la = 0)
        {
            $this->longueur = $lg;
            $this->largeur = $la;
        }

        // Calcule de la surface: longueur * largeur:
        function recupereSurface()
        {
            return ($this->longueur * $this->largeur);
        }

        // Calcul du périmètre: (longueur + largeur) * 2 :
        function recuperePerimetre()
        {
            return ( ($this->longueur + $this->largeur) * 2 );
        }

        // Test pour savoir si le rectangle est un carré
        function estCarre()
        {
            if ($this->longueur == $this->largeur)
            {
                return true; // Square
            }
            else
            {
                return false; // Pas un carré
            }
        }

    } // Fin de la classe Rectangle
?>
