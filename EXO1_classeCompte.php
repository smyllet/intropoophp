<?php
    class Compte
    {
        public $montant;

        public function virer($valeur, $destination)
        {

            $this->montant -= $valeur;
            $destination->montant += $valeur;

        }
    }

    // Insérez en dessous le code permettant de créer
    // une instance de Compte, nommée compteProfesseur
    $compteProfesseur = new Compte();

    //Initialisez son montant à la valeur de 100
    $compteProfesseur->montant = 100;

    //Créez un nouvel objet instance de la classe Compte nommé compteEleve
    $compteEleve = new Compte();

    //Initialisez son montant à la valeur de 100
    $compteEleve->montant = 100;

    //Effectuez un virement de 50 du compteEleve vers le compteProfesseur
    $compteEleve->virer(50, $compteProfesseur);

    echo "Compte professeur : ".$compteProfesseur->montant."<br>";
    echo "Compte élève : ".$compteEleve->montant;
?>
