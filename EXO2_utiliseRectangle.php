<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Rectangle</title>
</head>
<body>
    <?php
        // Inclusion de la classe Rectangle:
        require('EXO2_Rectangle.php');

        // Definition longueur et largeur:
        $longueur = 160;
        $largeur = 75;


        // Message bienvenue:
        echo "<h2>Etude de rectangle de longueur $longueur et largeur $largeur</h2>";

        // Création de rectangle:
        $rectangle = new Rectangle();

        // Initialisation du rectangle
        $rectangle->definitTaille($longueur,$largeur);

        // Impression aire.
        echo "<p>Ce rectagle à une surface de ".$rectangle->recupereSurface()."</p>";

        // Recupere le perimetre.
        echo "<p>Sont périmètre est de ".$rectangle->recuperePerimetre()."</p>";

        // Est ce un carre?
        echo '<p>Ce rectangle ';

        // instruction if à ajouter
        if ($rectangle->estCarre())
        {
            echo 'est aussi';
        }
        else
        {
            echo " n'est pas";
        }
        echo ' un carre.</p>';

        // Destruction du rectangle:
        unset($r);

    ?>
</body>
</html>
