<?php
    interface NomInterface
    {
        public function getNom();

        public function setNom($nom);
    }

    class Livre implements NomInterface
    {
        private $nomLivre;

        function __construct($nom)
        {
            $this->nomLivre = $nom;
        }

        public function getNom()
        {
            return $this->nomLivre;
        }

        public function setNom($nom)
        {
            $this->nomLivre = $nom;
        }

        public function __toString()
        {
            return "Le nom du livre est ".$this->getNom();
        }
    }

    $monLivrePrefere = new Livre("Harry Potter et le Prince de sang-mêlé");
    echo $monLivrePrefere;
?>