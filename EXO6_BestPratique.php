<?php
    interface NomInterface
    {
        public function getNom();

        public function setNom($nom);
    }

    trait NomTrait
    {
        private $nomLivre;

        public function getNom()
        {
            return $this->nomLivre;
        }

        public function setNom($nom)
        {
            return $this->nomLivre = $nom;
        }
    }

    class Livre implements NomInterface
    {
        use NomTrait;

        function __construct($nom)
        {
            $this->nomLivre = $nom;
        }

        public function __toString()
        {
            return "Le nom du livre est ".$this->getNom();
        }
    }

    $livre = new Livre("Harry Potter et le Prince de sang-mêlé");
    echo $livre;
?>