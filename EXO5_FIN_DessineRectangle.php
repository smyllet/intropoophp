<?php
require "EXO5_DEB_DefinitionFigures.php";

class dessineRectangle
{
  public $rose ;

  public $rectangles = array() ;

  function __construct (... $listeRectangle)
  {
    $this->rectangles=$listeRectangle ;
  }

  function listeRectangles()
  {
    foreach ($this->rectangles as $rectangle)
    {
      echo "$rectangle";
    }
  }

  function dessineMoi()
  {
    $canvas = imagecreatetruecolor(800, 800) ;
    $rose =  imagecolorallocate($canvas, 255, 105, 180) ;

    foreach ($this->rectangles as $rectangle)
    {
      imagerectangle($canvas, $rectangle->getPointHautGauche()->getAbscisse(), $rectangle->getPointHautGauche()->getOrdonnee(), $rectangle->getPointBasDroite()->getAbscisse(), $rectangle->getPointBasDroite()->getOrdonnee(), $rose);
    }

    header('Content-Type: image/jpeg');

    imagejpeg($canvas);
    imagedestroy($canvas);
  }
}

// Créez 2 points et un rectangle
$premierPoint = new Point(10,40);
$secondPoint = new Point(50,90);
$premierRectangle = new Rectangle($premierPoint,$secondPoint);


// Créez 2 autres points et un nouveau rectangle
$troisiemePoint = new Point(50,120);
$quatriemePoint = new Point(30,200);
$secondRectangle = new Rectangle($troisiemePoint,$quatriemePoint);

//Dessiné rectangle
$unDessin = new DessineRectangle($premierRectangle,$secondRectangle);
$unDessin->dessineMoi();
?>
