<?php
require "EXO5_DEB_DefinitionFigures.php";

class dessineCercle
{
    public $blue;

    public $cercles = array();

    // Constructeur de cercles
    function __construct (... $listeCercles)
    {
        $this->cercles=$listeCercles;
    }

    function dessineMoi()
    {
        $canvas = imagecreatetruecolor(800, 800);
        $blue = imagecolorallocate($canvas, 0, 150, 255);

        foreach ($this->cercles as $cercle)
        {
            imageellipse($canvas, $cercle->getPointCentre()->getAbscisse(), $cercle->getPointCentre()->getOrdonnee(), $cercle->getRayon(), $cercle->getRayon(), $blue);
        }

        header('Content-Type: image/png');

        imagepng($canvas);
        imagedestroy($canvas);
    }
}

// création de 2 points , par exemple 100,300 et 300,400
$premierPoint = new Point(100,300);
$secondPoint = new Point(300,400);

// création de 2 cercles
$premierCercle = new Cercle($premierPoint, 150);
$secondCercle = new Cercle($secondPoint, 360);

// création et affichage du dessin
$unDessin = new dessineCercle($premierCercle, $secondCercle);
$unDessin->dessineMoi();

?>
