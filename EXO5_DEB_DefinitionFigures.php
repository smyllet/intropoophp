<?php
class Point
{
    private $abscisse;
    private $ordonnee;

    function __construct($uneAbscisse, $uneOrdonnee)
    {
      $this->abscisse = $uneAbscisse ;
      $this->ordonnee = $uneOrdonnee;
    }

    function getAbscisse(){
      return $this->abscisse ;
    }

    function getOrdonnee(){
      return $this->ordonnee ;
    }

    public function __toString()
    {
        return "L'abcisse du point est ".$this->abscisse." et son ordonnée est ".$this->ordonnee.".<br>";
    }
}

class Rectangle
{
    private $pointHautGauche;
    private $pointBasDroite;

    function __construct($premierPoint, $deuxiemePoint)
    {
        $this->pointHautGauche = $premierPoint ;
        $this->pointBasDroite = $deuxiemePoint;
    }

    function getPointHautGauche()
    {
        return $this->pointHautGauche ;
    }

    function getPointBasDroite()
    {
        return $this->pointBasDroite ;
    }

    public function __toString()
    {
        return "Le rectangle est défini par les deux point suivant le point haut gauche : ".$this->pointHautGauche."Et également le point bas droite : ".$this->pointBasDroite;
    }
}

class Cercle
{
    private $pointCentre;
    private $rayon ;

    function __construct ($point, $rayon){
        $this->pointCentre = $point ;
        $this->rayon = $rayon;
    }


    function getPointCentre()
    {
        return $this->pointCentre;
    }

    function getRayon()
    {
        return $this->rayon ;
    }

   // Calcul du périmètre
    function getPerimetre()
    {
        return 2*$this->getRayon()*pi();
    }

    // Calcul de la surface
    function getSurface()
    {
        return pi()*$this->getRayon()*$this->getRayon();
    }

}
?>
